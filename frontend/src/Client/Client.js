
 import axios from "axios";
 import ResponseDict from "./ResponseDict";

 class Client {

    constructor(url) {
        this.url = url;
    }

    talkToBackend(endpoint, parameters, callback) {
        var address = this.url + endpoint;
        axios.get(address, {
            params: parameters
        })
        .then(function (res) {
            const structuredResponse = new ResponseDict(res.data.statusCode, res.data.data);
            callback(structuredResponse);
        });
    }

 }

 export default Client;