

 class ResponseDict {

    constructor(statusCode, data) {
        this.statusCode = statusCode;
        this.data = data;
    }

    get dictionary() { // returns the format above
        return {
            "statusCode": this.statusCode,
            "data": this.data,
        };
    }

    setStatus(statusCode) {
        this.statusCode = statusCode;
    }

    setData(data) {
        this.data = data;
    }

 }

 export default ResponseDict;