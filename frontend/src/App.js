

import './App.css';
import React, { Component } from "reactn";
import { 
  Route, 
  HashRouter } from "react-router-dom";

import Home from "./Routes/Home/Home";
import Game from "./Routes/Game/Game";

class App extends Component {

  constructor(props) {
    super(props);
  }


  render () {
    return (
      <HashRouter>
        <div className="wrapper">
          <div className="content">
            <Route path="/game/:gameID" component={Game} />
            <Route exact path="/" component={Home} />
          </div>
        </div>
      </HashRouter>
    );
  }
}


export default App;
