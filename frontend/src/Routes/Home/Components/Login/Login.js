/**
 * Imports -- modules, packages, assets
 */
import React, { useState } from "react";
import {Link, Redirect}  from "react-router-dom";

import Client from "../../../../Client/Client";

import './Assets/Login.css';

class Login extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            username: "",
            password: "",
            errorMsg: "",
        };

        this.checkForm = this.checkForm.bind(this);
        this.usernameChange = this.usernameChange.bind(this);
        this.passwordChange = this.passwordChange.bind(this);
    }

    usernameChange(e) {
        if (e.target.value != "") {
            e.target.className = "login-form-field-input login-form-field-filled";
        } else {
            e.target.className = "login-form-field-input login-form-field-empty";
        }
        this.setState({
            username: e.target.value,
            errorMsg: "",
        });
    }
    passwordChange(e) {
        if (e.target.value != "") {
            e.target.className = "login-form-field-input login-form-field-filled";
        } else {
            e.target.className = "login-form-field-input login-form-field-empty";
        }
        this.setState({
            password: e.target.value,
            errorMsg: "",
        });
    }
   
    checkForm() {
        var valid = true;
        if (this.state.username == "") {
            valid = false;
        }
        if (this.state.password == "") {
            valid = false;
        }

        if (valid) {
            let u = this.state.username;
            let p = this.state.password;
            new Promise((resolve, reject) => {
                var client = new Client("http://localhost:8080");
                client.talkToBackend("/attemptLogin", {username: u, password: p}, function(response) {
                    resolve(response.data);
                });
            }).then((data) => {
                if (data == "FAILURE") {
                    this.setState({
                        errorMsg: "Username and Password did not match!",
                    });
                } else {
                    let stuff = JSON.parse(data);
                    localStorage.setItem("user_token", stuff.token);
                    localStorage.setItem("user_info", JSON.stringify(stuff.user_info));
                    window.location.reload(false);
                }
            });
        }
    }

    render() {

        return (
            <div id="login-form-wrapper">

                <div id="login-form-title">Login</div>

                <form id="login-form" onSubmit={this.checkForm}>
                    <div className="form-error-msg">{this.state.errorMsg}</div>
                    <div className="login-form-field-wrapper">
                        <input className="login-form-field-input login-form-field-empty" id="login-form-field-username" type="text" onChange={this.usernameChange}/>
                        <label htmlFor="login-form-field-username" className="login-form-field-label">Username</label>
                    </div>
                    <div className="login-form-field-wrapper">
                        <input className="login-form-field-input login-form-field-empty" id="login-form-field-password" type="password" onChange={this.passwordChange}/>
                        <label htmlFor="login-form-field-password" className="login-form-field-label">Password</label>
                    </div>
                    <div className="form-submit-button-wrapper">
                        <button className="form-submit-button" type="submit">Log In</button>
                    </div>
                </form>
            </div>
        );
    }

}

export default Login;
