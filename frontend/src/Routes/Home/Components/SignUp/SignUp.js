/**
 * Imports -- modules, packages, assets
 */
import React, { useState } from "react";
import {Link, Redirect}  from "react-router-dom";

import Client from "../../../../Client/Client";

import './Assets/SignUp.css';

class SignUp extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            username: "",
            password: "",
            confirmPassword: "",
            errorMsg: "",
        };

        this.checkForm = this.checkForm.bind(this);
        this.usernameChange = this.usernameChange.bind(this);
        this.passwordChange = this.passwordChange.bind(this);
        this.confirmPasswordChange = this.confirmPasswordChange.bind(this);
    }

    usernameChange(e) {
        if (e.target.value != "") {
            e.target.className = "signup-form-field-input signup-form-field-filled";
        } else {
            e.target.className = "signup-form-field-input signup-form-field-empty";
        }
        this.setState({
            username: e.target.value,
            errorMsg: "",
        });
    }
    passwordChange(e) {
        if (e.target.value != "") {
            e.target.className = "signup-form-field-input signup-form-field-filled";
        } else {
            e.target.className = "signup-form-field-input signup-form-field-empty";
        }
        this.setState({
            password: e.target.value,
            errorMsg: "",
        });
    }

    confirmPasswordChange(e) {
        if (e.target.value != "") {
            e.target.className = "signup-form-field-input signup-form-field-filled";
        } else {
            e.target.className = "signup-form-field-input signup-form-field-empty";
        }
        this.setState({
            confirmPassword: e.target.value,
            errorMsg: "",
        });
    }
   
    checkForm() {
        var valid = true;
        if (this.state.username == "" || this.state.password == "" || this.state.confirmPassword == "") {
            valid = false;
            this.setState({
                errorMsg: "Missing field!",
            });
        }
        if (this.state.confirmPassword != this.state.password) {
            valid = false;
            this.setState({
                errorMsg: "Passwords do not match!",
            });
        }

        if (valid) {
            let u = this.state.username;
            let p = this.state.password;
            let c = this.state.confirmPassword;

            new Promise((resolve, reject) => {
                var client = new Client("http://localhost:8080");
                client.talkToBackend("/attemptSignUp", {username: u, password: p}, function(response) {
                    resolve(response.data);
                });
            }).then((data) => {
                let stuff = JSON.parse(data);
                if (stuff.status == "FAILURE") {
                    this.setState({
                        errorMsg: "Sign-up Failed!",
                    });
                } else {
                    localStorage.setItem("user_token", stuff.token);
                    localStorage.setItem("user_info", JSON.stringify(stuff.user_info));
                    window.location.reload(false);
                }
            });
        }
    }

    render() {

        return (
            <div id="signup-form-wrapper">

                <div id="signup-form-title">Sign Up</div>

                <form id="signup-form" onSubmit={this.checkForm}>
                    <div className="form-error-msg">{this.state.errorMsg}</div>
                    <div className="signup-form-field-wrapper">
                        <input className="signup-form-field-input signup-form-field-empty" id="signup-form-field-username" type="text" onChange={this.usernameChange}/>
                        <label htmlFor="signup-form-field-username" className="signup-form-field-label">Username</label>
                    </div>
                    <div className="signup-form-field-wrapper">
                        <input className="signup-form-field-input signup-form-field-empty" id="signup-form-field-password" type="password" onChange={this.passwordChange}/>
                        <label htmlFor="signup-form-field-password" className="signup-form-field-label">Password</label>
                    </div>
                    <div className="signup-form-field-wrapper">
                        <input className="signup-form-field-input signup-form-field-empty" id="signup-form-field-confirm" type="password" onChange={this.confirmPasswordChange}/>
                        <label htmlFor="signup-form-field-confirm" className="signup-form-field-label">Confirm Password</label>
                    </div>
                    <div className="form-submit-button-wrapper">
                        <button className="form-submit-button" type="submit">Sign Up</button>
                    </div>
                </form>
            </div>
        );
    }

}

export default SignUp;
