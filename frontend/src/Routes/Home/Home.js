/**
 * Imports -- modules, packages, assets
 */
import React from "react";
import {Link, Redirect}  from "react-router-dom";

import Login from './Components/Login/Login';
import SignUp from './Components/SignUp/SignUp';

import './Assets/Home.css';
import Client from '../../Client/Client';

class Home extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            gameStatus: "NONE",
            requestID: -1,
            formToggle: true,
        };
        this.requestGame = this.requestGame.bind(this);
        this.cancelRequest = this.cancelRequest.bind(this);
        this.toggleForm = this.toggleForm.bind(this);
    }

    cancelRequest() {

        // new Promise((resolve, reject) => {
        //     var client = new Client("http://localhost:8080");
        //     client.talkToBackend("/cancelRequest", {requestID: this.state.requestID}, function(response) {
        //         resolve(response.data);
        //     });
        // }).then((data) => {
        //     this.setState({
        //         gameStatus: "NONE",
        //         requestID: -1,
        //     });
        // });

        this.setState({
            gameStatus: "NONE",
        }, () => {
            const socket = new WebSocket("ws://localhost:9090");
            socket.context = this;
            socket.addEventListener('open', function(event) {
                let data = {
                    type: "CANCEL_REQUEST",
                    data: {
                        requestID: socket.context.state.requestID,
                    },
                };
                socket.send(JSON.stringify(data));
            });
            socket.addEventListener('message', function(event) {
                socket.context.setState({
                    requestID: -1,
                }, () => {
                    console.log("cancellation response: " + event.data);
                    socket.close();
                });
            });
        });
    }

    requestGame() {

        let userID = -1;
        let user_token = "";
        try {
            let user_info = JSON.parse(localStorage.getItem("user_info"));
            userID = user_info.userID;
            if (localStorage.getItem("user_token") != null) {
                user_token = localStorage.getItem("user_token");
            }
        } catch (error) {
            console.error("ERROR: BAD LOGIN INFO.");
            return;
        }
        

        this.setState({
            gameStatus: "WAITING",
        }, () => {
            const socket = new WebSocket("ws://localhost:9090");
            socket.context = this;
            socket.addEventListener('open', function(event) {
                let data = {
                    type: "MATCH_REQUEST",
                    data: {
                    timeControl: -1,
                    asColor: -1,
                    token: user_token,
                    },
                };
                socket.send(JSON.stringify(data));
            });
            socket.addEventListener('message', function(event) {

                let msg = JSON.parse(event.data);

                switch (msg.type) {
                    case "ERROR":
                        socket.context.setState({
                            gameStatus: "NONE",
                        });
                        console.error(msg.data);
                        break;
                    case "REQUEST_ID":
                        socket.context.setState({
                            requestID: msg.data.id,
                        }, () => {
                            console.log("request ID: " + socket.context.state.requestID);
                        });
                        break;
                    case "GAME_ID":
                        socket.context.props.history.push('/game/' + msg.data.id);
                        socket.close();
                        break;
                    default:
                        break;
                }
            });
        });
    }
    

    logout() {
        localStorage.removeItem("user_token");
        localStorage.removeItem("user_info");
        window.location.reload(false);
    }

    toggleForm() {
        this.setState({
            formToggle: !this.state.formToggle,
        });
    }

    render() {

        let b = <button onClick={this.requestGame}>Request a Game</button>;

        if (this.state.gameStatus === "WAITING") {
            b = <button onClick={this.cancelRequest}>Cancel Game Request</button>;
        }

        let u = localStorage.getItem("user_info");
        if (u != undefined) {
            u = JSON.parse(u);

            let accountInfo = "Logged in as " + u.username;
            // render logged in version of page
            return (
                <div>
                <h1>Welcome to AdamChess</h1>
                <br />
                <br />
                {accountInfo}        
                <br />
                <br />
                {b}

                <button id="log-out-button" onClick={this.logout}>Log Out</button>

            </div>
            );
        }

        // render logged out version of page
        return (
            <div className="home-page-wrapper-logged-out">
                <h1>Welcome to AdamChess</h1>
                <br />
                <br />
                <div id="form-picker-wrapper">
                    <div className="form-picker-button-wrapper">
                        <button className={"form-picker-button form-picker-button-enabled-" + !this.state.formToggle} disabled={this.state.formToggle} onClick={this.toggleForm}>Log In</button>
                        <button className={"form-picker-button form-picker-button-enabled-" + this.state.formToggle} disabled={!this.state.formToggle} onClick={this.toggleForm}>Sign Up</button>
                    </div>
                    <div className={"form-wrapper form-wrapper-vis-" + this.state.formToggle}>
                    <Login />
                    </div>
                    <div className={"form-wrapper form-wrapper-vis-" + !this.state.formToggle}>
                    <SignUp />
                    </div>
                </div>
            </div>
        );
    }

}

export default Home;
