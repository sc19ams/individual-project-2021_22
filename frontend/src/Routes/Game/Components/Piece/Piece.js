/**
 * Imports -- modules, packages, assets
 */
import React from "react";

import "./Assets/Piece.css";

import wP from "./Assets/Images/wP.svg";
import wN from "./Assets/Images/wN.svg";
import wB from "./Assets/Images/wB.svg";
import wR from "./Assets/Images/wR.svg";
import wQ from "./Assets/Images/wQ.svg";
import wK from "./Assets/Images/wK.svg";

import bP from "./Assets/Images/bP.svg";
import bN from "./Assets/Images/bN.svg";
import bB from "./Assets/Images/bB.svg";
import bR from "./Assets/Images/bR.svg";
import bQ from "./Assets/Images/bQ.svg";
import bK from "./Assets/Images/bK.svg";

class PieceComponent extends React.Component {



    constructor(props) {
        super(props);
        
        this.type = this.props.type;
        this.icon = this.getIcon();

    }

    getIcon() {
        switch(this.props.type) {
            case "p": return bP;
            case "P": return wP;
            case "b": return bB;
            case "B": return wB;
            case "n": return bN;
            case "N": return wN;
            case "r": return bR;
            case "R": return wR;
            case "q": return bQ;
            case "Q": return wQ;
            case "k": return bK;
            case "K": return wK;
            default:
                return null;
        }
    }


    PieceIcon(props) {
        var icon = props.icon;
        if (icon == null) {
            return <></>;
        }
        return <img src={icon}/>;
    }

    render() {
        return (
            <div className="piece-wrapper">
                <this.PieceIcon icon={this.icon}/>
            </div>
        );
    }

}

export default PieceComponent;
