/**
 * Imports -- modules, packages, assets
 */
import React from "react";

import "./Assets/Board.css";
import PieceComponent from "../Piece/Piece";

class SquareComponent extends React.Component {

    constructor(props) {
        super(props);

    }

    render() {

        return (
            <div className={"sq sq-flipped-" + this.props.flipped + " sq-" + this.props.color + " sq-file-" + this.props.name[0] + 
            " sq-row-" + this.props.name[1] + 
            " sq-selected-" + this.props.selected + " sq-valid-" + this.props.validMove + " sq-premoveFrom-" + this.props.premoveFrom + 
            " sq-premoveTo-" + this.props.premoveTo + " sq-recentFrom-" + this.props.recentFrom + " sq-recentTo-" + this.props.recentTo + " sq-checkMarker-" + this.props.checkMarker} onClick={
                () => {this.props.onClick(this.props.index)
                }}>

                <span>{this.props.name}</span>

                <PieceComponent type={this.props.contents}/>
            </div>
        );
    }

}

export default SquareComponent;
