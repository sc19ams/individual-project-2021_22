/**
 * Imports -- modules, packages, assets
 */
import React from "react";

import "./Assets/Board.css";
import SquareComponent from "./Square";

import Client from "../../../../Client/Client";

import moveSound from "./Assets/moveSound.mp3";

import ChessLogic from "../../../../ChessLogic/ChessLogic";

class BoardComponent extends React.Component {



    constructor(props) {
        super(props);
        
        this.squares = new Array(64);

        this.logician = new ChessLogic();

        this.socket = 0;

        this.state = {
            board: this.props.boardPos,
            selectedSquare: -1,
            selectedPiece: "",
            validMoves: [],
            perspective: this.props.perspective,
            game_info: {},
            premove: {move: "", name: ""},
            recentMove: {from: -1, to: -1},
            checkMarker: -1,
            gameOverMsg: "",
            gameOver: false,
        };

        this.moveSound = new Audio(moveSound);
    
        this.flipBoard = this.flipBoard.bind(this);
        this.initializeBoard = this.initializeBoard.bind(this);
        this.makeMove = this.makeMove.bind(this);
        this.viewBoard = this.viewBoard.bind(this);
    }

    componentDidMount() {
        this.initializeBoard();
        this.connectToGame();
    }
    
    connectToGame() {
        const socket = new WebSocket("ws://localhost:9090");
        socket.context = this;
        socket.addEventListener('open', function(event) {
            let data = {
                type: "JOIN_GAME",
                data: {
                    gameID: socket.context.props.gameID,
                },
            };
            this.send(JSON.stringify(data));
        });
        socket.addEventListener('message', function(event) {
            let msg = JSON.parse(event.data);

            switch(msg.type) {
                case "GAME_INFO":
                    socket.context.setState({
                        game_info: msg.data,
                    });
                    break;
                case "MAKE_MOVE":
                    socket.context.moveSound.play();
                    let move = msg.data.move;
                    socket.context.updateBoard(move);
                    if (socket.context.state.premove.move != "") {
                        socket.context.makeMove(socket.context.state.premove);
                        socket.context.setState({
                            premove: {move: "", name: ""},
                        });
                    }
                    break;
                default:
                    break;
            }
        });
        this.socket = socket;
    }

    updateBoard(move) {
        let moveParts = ("" + move.move).split(",");
        let i1 = moveParts[0];
        let i2 = moveParts[1];

        let newBoard = this.state.board;

        newBoard = newBoard.split('');

        let game_info = {...this.state.game_info};
        switch (parseInt(i1)) {
            case 0:
                game_info.whiteCanCastleQueen = false;
                break;
            case 4:
                game_info.whiteCanCastleQueen = false;
                game_info.whiteCanCastleKing = false;
                break;
            case 7:
                game_info.whiteCanCastleKing = false;
                break;
            case 56:
                game_info.blackCanCastleQueen = false;
                break;
            case 60:
                game_info.blackCanCastleKing = false;
                game_info.blackCanCastleQueen = false;
                break;
            case 63:
                game_info.blackCanCastleKing = false;
                break;
            default:
                break;
        }

        game_info.moves.push(move);
        newBoard[i2] = newBoard[i1];
        newBoard[i1] = "-";

        if (move.name.indexOf("=") > -1) { // promote the pawn
            let promo_piece = move.name.slice(-1);
            if (game_info.currentTurn == 1) {
                promo_piece = promo_piece.toLowerCase();
            }
            newBoard[i2] = promo_piece;
        }

        // if its a castling move we need to move the rook as well
        if (move.name == "O-O") { 
            if (game_info.currentTurn == 0) { // white is castling
                newBoard[7] = "-"; // remove rook from corner
                newBoard[5] = "R"; // move rook to new square
            } else { // black is castling
                newBoard[63] = "-"; // remove rook from corner
                newBoard[61] = "r"; // move rook to new square
            }
        } else if (move.name == "O-O-O") {
            if (game_info.currentTurn == 0) { // white is castling
                newBoard[0] = "-"; // remove rook from corner
                newBoard[3] = "R"; // move rook to new square
            } else { // black is castling
                newBoard[56] = "-"; // remove rook from corner
                newBoard[59] = "r"; // move rook to new square
            }
        }


        newBoard = newBoard.join('');
        game_info.currentTurn = -1 * (game_info.currentTurn - 1);
        
        if (this.logician.isInCheck(newBoard, game_info.currentTurn, game_info)) {
            this.setState({
                checkMarker: this.logician.findKing(newBoard, game_info.currentTurn),
            });
        } else {
            this.setState({
                checkMarker: -1,
            });
        }
        if (this.logician.isCheckmated(newBoard, game_info.currentTurn, game_info)) {
            if(game_info.currentTurn == 0) { // white is checkmated
                this.setState({
                    gameOverMsg: "Black wins by checkmate!",
                    gameOver: true,
                });
            } else { // black is checkmated
                this.setState({
                    gameOverMsg: "White wins by checkmate!",
                    gameOver: true,
                });
            }
        }
        if (this.logician.isStalemated(newBoard, game_info.currentTurn, game_info)) {
            this.setState({
                gameOverMsg: "Game is drawn by stalemate!",
                gameOver: true,
            });
        }

        this.setState( {
            board: newBoard,
            game_info: game_info,
            recentMove: {from: i1, to: i2},
        }, () => {
            this.initializeBoard();
            return;
        });
    }


    makeMove(move) {
        let msg = {
            type: "MAKE_MOVE",
            data: {
                move: move,
                moveColor: this.props.playingAs,
                gameID: this.props.gameID,
                token: localStorage.getItem("user_token"),
            },
        };
        this.socket.send(JSON.stringify(msg));
    }

    initializeBoard() {
        this.createSquares();
        this.forceUpdate();
    }

    getPieceFromSetup(i) {
        var setup = this.state.board.split('');

        return setup[i];

    }

    flipBoard() {
        this.setState({
            perspective: -1 * (this.state.perspective - 1), // swaps 0 to 1 and vice versa
        }, () => {
            this.initializeBoard();
        });
    }

    getSquareContents(i) {

        return this.state.board.charAt(i);
    }

    createSquares() {
        //let files = ["a", "b", "c", "d", "e", "f", "g", "h"];
        let c = "b";
        let premoveFrom = false;
        let premoveTo = false;
        let recentFrom = false;
        let recentTo = false;
        let checkMarker = false;

        for (let i = 0; i < 64; i++)
        {
            if (this.state.premove.move != "") {
                let parts = this.state.premove.move.split(",");
                premoveFrom = parseInt(parts[0]) == i;
                premoveTo = parseInt(parts[1]) == i;
            }
            recentFrom = parseInt(this.state.recentMove.from) == i;
            recentTo = parseInt(this.state.recentMove.to) == i;
            checkMarker = this.state.checkMarker == i;
            let x = i%8;
            let y = Math.floor(i/8);

            if (x % 2 === y % 2) { 
                c = "b";
            } else c = "w";
            var validMove = false;
            let counter;
            for (counter = 0; counter < this.state.validMoves.length; counter++) {
                let m = this.state.validMoves[counter];
                if (i == m.index) {
                    validMove = true;
                    break;
                }
            }
            var selected = this.state.selectedSquare == i;
            var contents = this.getSquareContents(i);
            this.squares[i] = <SquareComponent 
                key={i + contents} board={this} color={c} 
                name={this.logician.getSquareName(i)} onClick={() => {this.onSquareClick(i)}} 
                contents={contents} selected={selected} validMove={validMove} premoveFrom={premoveFrom} 
                premoveTo={premoveTo} flipped={this.state.perspective == 1} recentFrom={recentFrom} recentTo={recentTo} checkMarker={checkMarker}/>;
        }
        this.forceUpdate();
    }

    setSelection(index) {
        if (index === -1) {
            this.setState({
                selectedSquare: index,
                selectedPiece: "",
                validMoves: [],
            }, () => {
                this.createSquares();
                return;
            });
        }
        else {
            this.setState({
                selectedSquare: index,
                selectedPiece: this.squares[index].contents,
            }, () => {
                this.getValidMoves(index);
                return;
            });
        }
    }

    getValidMoves(index) {
        let checkForChecks = this.props.playingAs == this.state.game_info.currentTurn;
        let validMoves = this.logician.getValidMoves(this.state.board, index, checkForChecks, this.state.game_info);
        this.setState({
            validMoves: validMoves,
        }, () => {
            this.createSquares();
            return;
        });
    }

    setPremove(move) {
        let i = this.state.selectedSquare;
        move.name = move.name.replace("?", "Q"); // auto-promote to queen if premoved
        this.setState({
            premove: {
                move: i + "," + move.index,
                name: move.name,
            },
        });
    }

    onSquareClick(index) {
        if (this.state.premove.move != "") {
            this.setState({
                premove: {
                    move: "", name: "",
                },
            }, () => {
                this.initializeBoard();
            });
        }

        if (this.state.selectedSquare === -1) {
            if (this.logician.isFriendlyPiece(this.state.board, index, this.props.playingAs)) {
                this.setSelection(index);
            }
            return;
        }

        if (this.state.selectedSquare === index) {
            this.setSelection(-1);
            return;
        }

        var valid = false;
        let move = undefined;
        let counter;
        for (counter = 0; counter < this.state.validMoves.length; counter++) {
            let m = this.state.validMoves[counter];
            if (index == m.index) {
                valid = true;
                move = m;
                break;
            }
        }

        if (!valid) {
            if (this.logician.isFriendlyPiece(this.state.board, index, this.props.playingAs)) {
                this.setSelection(index);
                return;
            }
            this.setSelection(-1);
            return;
        }
        if (this.state.game_info.currentTurn != this.props.playingAs) {
            this.setPremove(move);
            this.setSelection(-1);
            return
        }

        move.name = move.name.replace("?", "Q"); // auto-promote to queen
        this.makeMove({
            move: this.state.selectedSquare + "," + index, 
            name: move.name,
        });
        this.setSelection(-1);
        return;

    }

    viewBoard() {
        this.setState({gameOverMsg: ""});
    }

    render() {

        let whiteMoves = [];
        let blackMoves = [];
        let moveNumbers = [];
        try {
            let i;
            for (i = 0; i < this.state.game_info.moves.length; i++) {
                if (i % 2 == 0) { // white move
                moveNumbers.push(<li key={(i/2) + 1} className="gi-move-number-list-item">{(i/2) + 1}</li>);
                    whiteMoves.push(<li key={i} className="gi-moves-list-item">{this.state.game_info.moves[i].name}</li>);
                } else { // black move
                    blackMoves.push(<li key={i} className="gi-moves-list-item">{this.state.game_info.moves[i].name}</li>);
                }
            }
        } catch(err){}

        let gameOverClass = "board-gameover-wrapper-show";
        if (this.state.gameOverMsg == "") {
            gameOverClass = "board-gameover-wrapper-hide";
        }

        return (
            <div className="board-outer-wrapper">
                <div className="board-inner-wrapper">
                    <div className={"board-gameover-input-blocker-" + this.state.gameOver}></div>
                    <div className={"board-gameover-wrapper " + gameOverClass}>
                        <div className="board-gameover-message">
                            {this.state.gameOverMsg}
                        </div>
                        <button className="board-gameover-button" onClick={this.viewBoard}>
                            View Board
                        </button>
                        <button className="board-gameover-button">
                            Home
                        </button>
                    </div>
                    <div className="board-wrapper">
                        {this.squares}
                    </div>
                </div>
                <div className="game-info-wrapper">
                    <div className="gi-move-lists-wrapper">
                        <div className="gi-move-lists-label">
                            Moves
                        </div>
                        <div className="gi-move-number-wrapper">
                            <ul className="gi-move-number-list">
                                {moveNumbers}
                            </ul>
                        </div>
                        <div className="gi-moves-list-wrapper">
                            <ol className="gi-moves-list">
                                {whiteMoves}
                            </ol>
                        </div>
                        <div className="gi-moves-list-wrapper">
                            <ol className="gi-moves-list">
                                {blackMoves}
                            </ol>
                        </div>
                    </div>
                </div>
                <button onClick={this.flipBoard}>Flip</button>
            </div>
        );
    }

}

export default BoardComponent;
