/**
 * Imports -- modules, packages, assets
 */
import React from "react";

import "./Assets/Game.css";
import Client from "../../Client/Client";

import BoardComponent from "./Components/Board/Board";

class Game extends React.Component {

    constructor(props) {
        super(props);
        this.gameID = props.match.params.gameID;

        this.state = {
            players: [],
            boardPos: "NOT_SET",
            playingAs: -2,
        };

    }

    componentDidMount(){
        this.getPlayers();
        this.getPosition();
    }

    getPosition(){
        
        new Promise((resolve, reject) => {
            var client = new Client("http://localhost:8080");
            client.talkToBackend("/getPosition", {gameID: this.gameID}, function(response) {
                console.log(response);
                resolve(response.data);
            });
        }).then((data) => {
            this.setState({
                boardPos: data,
            }, () => {
                this.forceUpdate();
            });
        });
    }

    getPlayers() {
        new Promise((resolve, reject) => {
            var client = new Client("http://localhost:8080");
            client.talkToBackend("/getPlayers", {gameID: this.gameID}, function(response) {
                resolve(response.data);
            });
        }).then((data) => {
            new Promise((resolve, reject) => {
                var client = new Client("http://localhost:8080");
                client.talkToBackend("/verifyLogin", {token: localStorage.getItem("user_token")}, function(response) {
                    resolve(response.data);
                });
            }).then((validLogin) => {
                if (!validLogin) {
                    console.error("ERROR: LOGIN VERIFICATION FAILED!");
                } else {
                    let isWhite = validLogin.userID == data[0].userID;
                    let isBlack = validLogin.userID == data[1].userID;
                    if (isWhite && isBlack) {
                        this.setState({
                            playingAs: 2,
                        });
                    } else if (isWhite) {
                        this.setState({
                            playingAs: 0,
                        });
                    } else if (isBlack) {
                        this.setState({
                            playingAs: 1,
                        });
                    } else {
                        this.setState({
                            playingAs: -1,
                        });
                    }
                }
                this.setState({
                    players: data,
                });
            });
        });
    }

    render() {

        let white = "";
        let black = "";
        let whiteStyle = {};
        let blackStyle = {};
        let usernameHighlight = {color:"red"};
        let perspective = 0; // default is white perspective
        
        switch(this.state.playingAs) {
            case 0:
                whiteStyle = usernameHighlight;
                break;
            case 1:
                blackStyle = usernameHighlight;
                perspective = 1; // black perspective
                break;
            case 2:
                whiteStyle = usernameHighlight;
                blackStyle = usernameHighlight;
                break;
            default:
                break;
        }

        try {
            white = this.state.players[0]["username"];
            black = this.state.players[1]["username"];
        } catch (e){}
        if (this.state.boardPos === "NOT_SET" || this.state.playingAs == -2) {
            return (<div></div>);
        }
        return (
            <div>
                game: {this.gameID}
                <div>
                    <BoardComponent gameID={this.gameID} boardPos={this.state.boardPos} playingAs={this.state.playingAs} perspective={perspective}/>
                </div>
                players: <span style={whiteStyle}>{white}</span> vs <span style={blackStyle}>{black}</span>
            </div>
        );
    }

}

export default Game;
