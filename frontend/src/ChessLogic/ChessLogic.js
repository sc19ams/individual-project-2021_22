

class ChessLogic {

    isFriendlyPiece(board, i, side) {


        if (side == -1) return false; // playing neither side
        if (side == 2) return true; // playing both sides

        if (board.charAt(i) == "-") return false;

        var pieceSide = 1; // assume black
        if (board.charAt(i) == board.charAt(i).toUpperCase()) pieceSide = 0; // its white

        return pieceSide == side;
    }

    getRookMoves() {
        var result = new Array(4);
        result[0] = [];
        result[1] = [];
        result[2] = [];
        result[3] = [];

        for (var i = 1; i < 8; i++) {
            result[0].push({dx: -i, dy: 0}); // moving left
            result[1].push({dx: i, dy: 0}); // moving right
            result[2].push({dx: 0, dy: -i}); // moving down
            result[3].push({dx: 0, dy: i}); // moving up
        }

        return result;
    }

    getBishopMoves() {
        var result = new Array(4);
        result[0] = [];
        result[1] = [];
        result[2] = [];
        result[3] = [];

        for (var i = 1; i < 8; i++) {
            result[0].push({dx: -i, dy: i}); // moving up-left
            result[1].push({dx: i, dy: i}); // moving up-right
            result[2].push({dx: -i, dy: -i}); // moving down-left
            result[3].push({dx: i, dy: -i}); // moving down-right
        }

        return result;
    }

    getKnightMoves() {
        var result = [];

        var counter = 0;
        for (var i = -1; i < 2; i+=2) {
            for (var j = -1; j < 2; j+=2) {
                result.push([]);
                result[counter++].push({dx: i*2, dy: j});
                result.push([]);
                result[counter++].push({dx: i, dy: j*2});
            }
        }

        return result;
    }

    getQueenMoves() {
        var rook = this.getRookMoves();
        var bishop = this.getBishopMoves();

        var result = new Array(8);
        result[0] = [];
        result[1] = [];
        result[2] = [];
        result[3] = [];
        result[4] = [];
        result[5] = [];
        result[6] = [];
        result[7] = [];

        for (var i = 0; i < 4; i++) {
            result[i] = rook[i];
            result[i+4] = bishop[i];
        }


        return result;
    }

    getKingMoves(board, index, canCastleKing, canCastleQueen) {
        var result = new Array(10);
        result[0] = [];
        result[1] = [];
        result[2] = [];
        result[3] = [];
        result[4] = [];
        result[5] = [];
        result[6] = [];
        result[7] = [];
        result[8] = [];
        result[9] = [];

        var counter = 0;
        for (var i = -1; i < 2; i++) {
            for (var j = -1; j < 2; j++) {
                if (i == 0 && j == 0) continue;
                result[counter++].push({dx: i, dy: j});
            }
        }

        if (index == 4) {
            if (canCastleKing && this.isOccupied(board, index, index + 1) == 0 && this.isOccupied(board, index, index + 2) == 0) {
                result[counter++].push({dx: +2, dy: 0, name:"O-O"});
            }
            if (canCastleQueen && this.isOccupied(board, index, index - 1) == 0 && this.isOccupied(board, index, index - 2) == 0) {
                result[counter++].push({dx: -2, dy: 0, name:"O-O-O"});
            }
        } else {
            if (canCastleQueen && this.isOccupied(board, index, index - 1) == 0 && this.isOccupied(board, index, index - 2) == 0) {
                result[counter++].push({dx: -2, dy: 0, name:"O-O-O"});
            }
            if (canCastleKing && this.isOccupied(board, index, index + 1) == 0 && this.isOccupied(board, index, index + 2) == 0) {
                result[counter++].push({dx: +2, dy: 0, name:"O-O"});
            }
        }

        return result;
    }

    // gets a list of squares which the piece at square 'i' could access on an empty board
    getAccessibleSquares(piece, i, board, game_info) {
        var allMoves;

        switch(piece) {
            case "q":
            case "Q":
                allMoves = this.getQueenMoves();
                break;
            case "r":
            case "R":
                allMoves = this.getRookMoves();
                break;
            case "b":
            case "B":
                allMoves = this.getBishopMoves();
                break;
            case "n":
            case "N":
                allMoves = this.getKnightMoves();
                break;
            case "k":
                allMoves = this.getKingMoves(board, i, game_info.blackCanCastleKing, game_info.blackCanCastleQueen);
                break;
            case "K":
                allMoves = this.getKingMoves(board, i, game_info.whiteCanCastleKing, game_info.whiteCanCastleQueen);
                break;
            default:
                allMoves = [];
                break;
        }

        let x = i%8;
        let y = Math.floor(i/8);

        var result = [];

        for (var j = 0; j < allMoves.length; j++) {
            result.push([]);
            for (var k = 0; k < allMoves[j].length; k++) {

                let newX = x + allMoves[j][k].dx;
                let newY = y + allMoves[j][k].dy;

                let move = {
                    index: newX + newY*8,
                    name: "",
                };

                if (allMoves[j][k].name != null) {
                    move.name = allMoves[j][k].name;
                }

                if (newX >= 0 && newX < 8 && newY >= 0 && newY < 8) {
                    result[j].push(move);
                }
            }
        }

        return result;
    }

    getPositionAfterMove(board, i1, move) {
        let i2 = move.index;

        let newBoard = board;

        newBoard = newBoard.split('');

        newBoard[i2] = newBoard[i1];
        newBoard[i1] = "-";

        // if its a castling move we need to move the rook as well
        if (move.name == "O-O") { 
            if (i1 == 4) { // white is castling
                newBoard[7] = "-"; // remove rook from corner
                newBoard[5] = "R"; // move rook to new square
            } else { // black is castling
                newBoard[63] = "-"; // remove rook from corner
                newBoard[61] = "r"; // move rook to new square
            }
        } else if (move.name == "O-O-O") {
            if (i1 == 4) { // white is castling
                newBoard[0] = "-"; // remove rook from corner
                newBoard[3] = "R"; // move rook to new square
            } else { // black is castling
                newBoard[56] = "-"; // remove rook from corner
                newBoard[59] = "r"; // move rook to new square
            }
        }


        newBoard = newBoard.join('');
        return newBoard;
    }

    // returns -1 for enemy occupation, 0 for no occupation, 1 for friendly occupation
    isOccupied(board, i, j) {

        if (board.charAt(j) == "-") return 0; // not occupied

        // if both white or both black, return 1
        if ((board.charAt(i) == board.charAt(i).toUpperCase()) == (board.charAt(j) == board.charAt(j).toUpperCase())) return 1;

        // must have enemy piece, return -1
        return -1; 
    }

    getValidMoves(board, i, removeIfInCheck, game_info) {

        var x = i%8;
        var y = Math.floor(i/8);

        var piece = board.charAt(i);

        if (piece == "p" || piece == "P") { // pawns are weird so we'll deal with them separately
            let result = [];
            let suffix = "";
            if (piece == "p") {
                if (Math.floor((i-8)/8) == 0) {
                    suffix = "=?"; // add promotion to end of move name
                }
                if (board.charAt(i-8) == "-") {
                    result.push({
                        index: i-8,
                        name: this.getSquareName(i-8) + suffix,
                    });
                    if (y == 6) {
                        if (board.charAt(i-16) == "-") {
                            result.push({
                                index: i-16,
                                name: this.getSquareName(i-16),
                            });
                        }
                    }
                }
                if (x-1 >= 0) {
                    if (board.charAt(i-9) != "-" && board.charAt(i-9).toUpperCase() == board.charAt(i-9)) {
                        result.push({
                            index: i-9,
                            name: this.getSquareName(i)[0] + "x" + this.getSquareName(i-9) + suffix,
                        });
                    }
                }
                if (x+1 < 8) {
                    if (board.charAt(i-7) != "-" && board.charAt(i-7).toUpperCase() == board.charAt(i-7)) {
                        result.push({
                            index: i-7,
                            name: this.getSquareName(i)[0] + "x" + this.getSquareName(i-7) + suffix,
                        });
                    }
                }
            } else {
                if (Math.floor((i+8)/8) == 7) {
                    suffix = "=?"; // add promotion to end of move name
                }
                if (board.charAt(i+8) == "-") {
                    result.push({
                        index: i+8,
                        name: this.getSquareName(i+8) + suffix,
                    });
                    if (y == 1) {
                        if (board.charAt(i+16) == "-") {
                            result.push({
                                index: i+16,
                                name: this.getSquareName(i+16),
                            });
                        }
                    }
                }
                if (x-1 >= 0) {
                    if (board.charAt(i+7) != "-" && board.charAt(i+7).toUpperCase() != board.charAt(i+7)) {
                        result.push({
                            index: i+7,
                            name: this.getSquareName(i)[0] + "x" + this.getSquareName(i+7) + suffix,
                        });
                    }
                }
                if (x+1 < 8) {
                    if (board.charAt(i+9) != "-" && board.charAt(i+9).toUpperCase() != board.charAt(i+9)) {
                        result.push({
                            index: i+9,
                            name: this.getSquareName(i)[0] + "x" + this.getSquareName(i+9) + suffix,
                        });
                    }
                }
            }
            let validMoves = [];

            if (removeIfInCheck) {
                let epicCounter;
                for (epicCounter = 0; epicCounter < result.length; epicCounter++) {
                    let move = result[epicCounter];
                    let newPosition = this.getPositionAfterMove(board, i, move);
                    if (!this.isInCheck(newPosition, game_info.currentTurn, game_info)) {
                        validMoves.push(move);
                    }
                }
                return validMoves;
            } else {
                return result;
            }
        }

        var accessibleSquares = this.getAccessibleSquares(piece, i, board, game_info);

        var nonBlockedSquares = [];

        for (let j = 0; j < accessibleSquares.length; j++) {
            for (let k = 0; k < accessibleSquares[j].length; k++) {
                var occupation = this.isOccupied(board, i, accessibleSquares[j][k].index);
                if (occupation == 0) {
                    if (accessibleSquares[j][k]["name"] == "") {
                        accessibleSquares[j][k]["name"] = piece.toUpperCase() + this.getSquareName(accessibleSquares[j][k].index);
                    }
                    nonBlockedSquares.push(accessibleSquares[j][k]);
                } else if (occupation == 1) {
                    break; // current movement path is blocked by friendly, don't add current or beyond
                } else {
                    if (accessibleSquares[j][k]["name"] == "") {
                        accessibleSquares[j][k]["name"] = piece.toUpperCase() + "x" + this.getSquareName(accessibleSquares[j][k].index);
                    }
                    nonBlockedSquares.push(accessibleSquares[j][k]);
                    break; // current movement path is blocked by enemy, don't add beyond current
                }
            }
        }

        let validMoves = [];

        if (removeIfInCheck) {
            let epicCounter;
            for (epicCounter = 0; epicCounter < nonBlockedSquares.length; epicCounter++) {
                let move = nonBlockedSquares[epicCounter];
                let newPosition = this.getPositionAfterMove(board, i, move);
                if (!this.isInCheck(newPosition, game_info.currentTurn, game_info)) {
                    validMoves.push(move);
                }
            }
            return validMoves;
        } else {
            return nonBlockedSquares;
        }
        
    }

    hasValidMoves(board, color, game_info) {
        let i;
        for (i = 0; i < 64; i++) {
            if (this.isFriendlyPiece(board, i, color)) {
                let moves = this.getValidMoves(board, i, true, game_info); // get moves this piece can make
                if (moves.length > 0) return true; // we have a valid move
            }
        }
        return false; // no valid moves found
    }

    findKing(board, color) {

        let i;
        for (i = 0; i < 63; i++) {
            if (color == 0) {
                if (board.charAt(i) == 'K') {
                    return i;
                }
            } else {
                if (board.charAt(i) == 'k') {
                    return i;
                }
            }
        }
        return -1;
    }

    isStalemated(board, color, game_info) {
        if (this.isInCheck(board, color, game_info)) { // can't be stalemate with check
            return false;
        }
        return !this.hasValidMoves(board, color, game_info); // if we don't have any valid moves, we're stalemated
    }

    isCheckmated(board, color, game_info) {
        if (!this.isInCheck(board, color, game_info)) { // can't be checkmated without check
            return false;
        }
        return !this.hasValidMoves(board, color, game_info); // if we don't have any valid moves, we're checkmated
    }

    isInCheck(board, color, game_info) {
        let enemyColor = -1*(color-1);
        let kingIndex = this.findKing(board, color);

        let i;

        for (i = 0; i < 63; i++) {
            if (this.isFriendlyPiece(board, i, enemyColor)) { // if there is enemy piece here
                let moves = this.getValidMoves(board, i, false, game_info);
                let j;
                for (j = 0; j < moves.length; j++) {
                    let move = moves[j];
                    if (move.index == kingIndex) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    getSquareName(index) {
        if (index < 0 || index > 63) {
            return false;
        }
        
        let files = ["a", "b", "c", "d", "e", "f", "g", "h"];
        return files[index%8] + Math.floor(index/8 + 1);
    }



}


export default ChessLogic;