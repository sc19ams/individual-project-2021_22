----------------------
Requirements to run:

JavaScript package manager 'npm'
mysql
--------------------

To run backend:
In terminal, navigate to 'backend' folder
Run command 'node index.js'

--------------------

To run frontend:
In separate terminal, navigate to 'frontend' folder
Run command 'npm start'

Open browser to http://localhost:3000
----------------------