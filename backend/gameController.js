

class GameController {

    constructor() {
        // constructor !!
    }


    processMove(dbConnection, gameID, moveFrom, moveTo, callback) {

        dbConnection.query("SELECT fen FROM games WHERE gameID=?", [gameID], function(err, result, fields) {

            let fen = result.fen;

            if (err) throw err;
            callback(result);
        });

        callback("you made a move: " + moveFrom + " to " + moveTo);
    }

    findMatch(requestID, requests, callback) {

        let req = requests[requestID][1];

        for (var id in requests) {
            if (id === requestID) {
                continue;
            }
            let r = requests[id][1];
            console.log(r);
            if (!(r.timeControl == -1 || req.timeControl == -1 || req.timeControl == r.timeControl)) {
                continue;
            }
            if (!(r.asColor == -1 || req.asColor == -1 || r.asColor != req.asColor)) {
                continue;
            }
            callback(id);
        }

        callback(-1);
    }
    

}

module.exports = GameController;