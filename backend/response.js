
 class ResponseDict {

    constructor(statusCode, data) {
        this.statusCode = statusCode;
        this.data = data; 
    }

    get dictionary() {
        return {
            "statusCode": this.statusCode,
            "successfulOperation": this.successfulOperation,
            "operation": this.operation,
            "message": this.message,
            "data": this.data
        };
    }

    setStatus(statusCode) {
        this.statusCode = statusCode;
    }
    setSuccessfulOperation(successfulOperation) {
        this.successfulOperation = successfulOperation;
    }
    setOperations(operation) {
        this.operation = operation;
    }
    setMessage(message) {
        this.message = message;
    }
    setData(data) {
        this.data = data;
    }

 }

module.exports = ResponseDict;