
class DBController {

    checkLoginDetails(dbConnection, username, password, callback) {
        dbConnection.query("SELECT userID, username FROM users WHERE username=? AND password=?", [username, password], function (err, result, fields) {
            if (err) throw err;
            
            if (result.length == 0) {
                callback("FAILURE");
                return;
            }

            let data = {
                userID: result[0].userID,
                username: result[0].username,
            };

            callback(data);
        });
    }

    checkUsernameExists(dbConnection, username, callback) {
        dbConnection.query("SELECT userID FROM users WHERE username=?", [username], function(err, result, fields) {
            callback(result.length > 0);
        });
    }

    createUser(dbConnection, username, password, callback) {
        dbConnection.query("INSERT INTO users (username, password) VALUES (?, ?)", [username, password], function(err, result, fields) {
            if (err) throw err;

            callback(result.insertId);
        });
    }


    getPlayersInGame(dbConnection, gameID, callback) {
        let whiteUser = "";
        let blackUser = "";

        dbConnection.query("SELECT whiteUser, blackUser FROM games WHERE gameID=?", [gameID], function(err, result, fields) {
            if (err) throw err;
            
            dbConnection.query("SELECT username FROM users WHERE userID=? UNION SELECT username FROM users WHERE userID=?", [result[0].whiteUser, result[0].blackUser], function(err2, res, fields) {
                if (err2) throw err2;

                let response = [{userID: result[0].whiteUser, username: res[0].username}, {userID: result[0].blackUser, username: res[1].username}];
                callback(response);
            });
        });
    }

    setGamePosition(dbConnection, gameID, move, callback) {

        let moveParts = move.move.split(",");

        dbConnection.query("SELECT position FROM games WHERE gameID=?", [gameID], function(err, result, fields) {
            if (err) throw err;
            let position = result[0].position;
            position = position.split('');
            position[moveParts[1]] = position[moveParts[0]];
            position[moveParts[0]] = "-";

            if (move.name.indexOf("=") > -1) { // promote the pawn
                let promo_piece = move.name.slice(-1);
                if (position[moveParts[1]] == position[moveParts[1]].toLowerCase()) {
                    promo_piece = promo_piece.toLowerCase();
                }
                position[moveParts[1]] = promo_piece;
            }

            position = position.join('');

            dbConnection.query("UPDATE games SET position=? WHERE gameID=?", [position, gameID], function(err, result, fields) {
                if (err) throw err;
                callback("moved");
            });
        });
    }

    getGamePosition(dbConnection, gameID, callback) {
        dbConnection.query("SELECT position FROM games WHERE gameID=?", [gameID], function(err, result, fields) {
            if (err) throw err;
            
            callback(result[0].position);
        });
    }

    createGame(dbConnection, user1Info, user2Info, callback) {

        let position = "RNBQKBNRPPPPPPPP--------------------------------pppppppprnbqkbnr";
        let whiteUser = -1;
        let blackUser = -1;

        if (user1Info.asColor == 0 || user2Info.asColor == 1) { // this user is white
            whiteUser = user1Info.userID;
            blackUser = user2Info.userID;
        } else if (user1Info == 1 || user2Info.asColor == 0) { // this user is black
            whiteUser = user2Info.userID;
            blackUser = user1Info.userID;
        } else { // neither has a preference -> random
            let rand = Math.random() < 0.5;
            if (rand) {
                whiteUser = user1Info.userID;
                blackUser = user2Info.userID;
            } else {
                whiteUser = user2Info.userID;
                blackUser = user1Info.userID;
            }
        }

        dbConnection.query("INSERT INTO games (position, whiteUser, blackUser) VALUES (?, ?, ?);", [position, whiteUser, blackUser], function(err, result, fields) {

            if (err) throw err;
            callback(result.insertId);
        });
    }

}

module.exports = DBController;