
// require .env file for storing API keys etc.
require('dotenv').config();


const cors = require('cors'); // middle package for express
const url = require('url');
const mysql = require('mysql');

const WebSocket = require('ws');
const wss = new WebSocket.Server({port: 9090}); 

const ResponseDict = require("./response.js");
const GameController = require('./gameController.js');
const DBController = require('./database/dbController.js');

const ChessLogic = require("./ChessLogic/ChessLogic.js");

// require various npm packages
const express = require('express');

const jwt = require('njwt');

const port = process.env.APP_PORT; // read from .env using process.env.<var name>
const app = express();

// Cors and body-parser
app.use(cors());
app.use(express.urlencoded({
    extended: true
})); // parse URL-encoded bodies (as sent by HTML forms)
app.use(express.json()); // parse JSON bodies (as sent by API clients)

const jwtKey = 'secret-key-sf48yY(Vy8ea9VBY(';

const gameController = new GameController();
const dbController = new DBController();
const chessLogic = new ChessLogic();

var dbConnection = mysql.createConnection({
  host: "localhost",
  user: "adam",
  password: "4D4M5P455W0RD",
  port: 3306,
  database: "chessDB",
});

dbConnection.connect(function(err) {
  if (err) throw err;
  console.log("Connected to Database!");
});

const NUM_TIME_CONTROLS = 3;

var requests = {}; // dictionary mapping unique IDs to connections

var gamesInProgress = {}; // dictionary mapping game IDs to arrays of connections

wss.on('connection', function connection(con) {
    
    con.connection_id = "";

    con.on('close', function(event){
        delete requests[con.connection_id];
    });

    con.onmessage = function (event) {
        let msg = JSON.parse(event.data);
        let response;
        switch(msg.type) {
            case "MAKE_MOVE":
                jwt.verify(msg.data.token, jwtKey, function(err, data) {

                    if (err) {
                        response = {type: "ERROR", data: "ERROR: LOGIN VERIFICATION FAILED."};
                        con.send(JSON.stringify(response));
                        validLogin = false;
                        return;
                    }

                    if (msg.data.moveColor == gamesInProgress[msg.data.gameID].currentTurn) {

                        response = {
                            type: "MAKE_MOVE",
                            data: {
                                move: msg.data.move,
                            },
                        };
                        dbController.setGamePosition(dbConnection, msg.data.gameID, msg.data.move, function(res) {
                            for (var connection of gamesInProgress[msg.data.gameID].connections) {
                                connection.send(JSON.stringify(response));
                            }
                            gamesInProgress[msg.data.gameID].moves.push(msg.data.move);
                            gamesInProgress[msg.data.gameID].currentTurn = -1 * (gamesInProgress[msg.data.gameID].currentTurn - 1); //swap turns
                            let moveParts = ("" + msg.data.move.move).split(",");
                            let i1 = moveParts[0];
                            switch (parseInt(i1)) {
                                case 0:
                                    gamesInProgress[msg.data.gameID].whiteCanCastleQueen = false;
                                    break;
                                case 4:
                                    gamesInProgress[msg.data.gameID].whiteCanCastleQueen = false;
                                    gamesInProgress[msg.data.gameID].whiteCanCastleKing = false;
                                    break;
                                case 7:
                                    gamesInProgress[msg.data.gameID].whiteCanCastleKing = false;
                                    break;
                                case 56:
                                    gamesInProgress[msg.data.gameID].blackCanCastleQueen = false;
                                    break;
                                case 60:
                                    gamesInProgress[msg.data.gameID].blackCanCastleKing = false;
                                    gamesInProgress[msg.data.gameID].blackCanCastleQueen = false;
                                    break;
                                case 63:
                                    gamesInProgress[msg.data.gameID].blackCanCastleKing = false;
                                    break;
                                default:
                                    break;
                            }
                        });
                    }
                });

                break;
            case "JOIN_GAME":
                gamesInProgress[msg.data.gameID].connections.push(con); // connect to the game room
                let game = {...gamesInProgress[msg.data.gameID]}; // copy game dictionary before removing irrelevant info
                delete game["connections"]; // send all the game info except connections
                delete game["premove"];
                response = {
                    type: "GAME_INFO",
                    data: game,
                };
                con.send(JSON.stringify(response)); // send game info to user
                break;
            case "MATCH_REQUEST":

                let validLogin = false;
                jwt.verify(msg.data.token, jwtKey, function(err, data) {
                    if (err) {
                        response = {type: "ERROR", data: "ERROR: LOGIN VERIFICATION FAILED."};
                        con.send(JSON.stringify(response));
                        validLogin = false;
                        return;
                    }
                    msg.data["userID"] = data.body.userID;

                    var uniqueID;

                    do {
                        uniqueID = Math.random().toString(36).substr(2, 9);
                    } while (uniqueID in requests);
                    con.connection_id = uniqueID;
                    requests[uniqueID] = [con, msg.data];

                    response = {
                        type: "REQUEST_ID",
                        data: {
                            id: uniqueID,
                        },
                    };

                    con.send(JSON.stringify(response));

                    gameController.findMatch(uniqueID, requests, function(matchingID) {
                        if (matchingID != -1) {
                            dbController.createGame(dbConnection, requests[matchingID][1], requests[uniqueID][1], function(res) {
                                gamesInProgress[res] = {};
                                gamesInProgress[res]["connections"] = new Array();
                                gamesInProgress[res]["currentTurn"] = 0;
                                gamesInProgress[res]["whiteCanCastleKing"] = true;
                                gamesInProgress[res]["whiteCanCastleQueen"] = true;
                                gamesInProgress[res]["blackCanCastleKing"] = true;
                                gamesInProgress[res]["blackCanCastleQueen"] = true;
                                gamesInProgress[res]["moves"] = [];

                                response = {
                                    type: "GAME_ID",
                                    data: {
                                        id: res,
                                    }
                                };
                                let data = JSON.stringify(response);
                                con.send(data);
                                requests[matchingID][0].send(data);

                                delete requests[uniqueID];
                                delete requests[matchingID];
                            });
                        }
                    });
                });
                break;
            case "CANCEL_REQUEST":
                delete requests[msg.data.requestID];
                con.send("REQUEST CANCELLED");
                break;
            default:
                con.send("ERROR: UNKNOWN MSG TYPE");
                break;
        }
    }
});

// CRUD Methods (req = url parameter querying, res = response sender)
app.get('/', (req, res) => { // localhost:8080/

    // Response builder
    const response = new ResponseDict();
    response.setStatus(200); // common status codes found online
    response.setSuccessfulOperation(true); // operation processed successfully
    response.setOperations(0); // basic operation 0, test if listener works
    response.setMessage(`Server is listening on port ${port}`);
    response.setData({
        "Hello": "World"
    });

    // Send response using the res parameter
    res.json(response.dictionary);
});

app.get('/verifyLogin', (req, res) => {
    var q = url.parse(req.url, true);
    var qData = q.query;

    jwt.verify(qData.token, jwtKey, function(err, data) {
        if (err) {
            let response = new ResponseDict(400, false);
            res.json(response.dictionary);
        } else {
            let response = new ResponseDict(400, data.body);
            res.json(response.dictionary);
        }
    });
});

app.get('/getPlayers', (req, res) => {

    var q = url.parse(req.url, true);
    var qData = q.query;

    var gameID = qData.gameID;

    dbController.getPlayersInGame(dbConnection, gameID, function(data) {

        let response = new ResponseDict(400, data);
        res.json(response.dictionary);
    });

});

app.get('/getPosition', (req, res) => {

    var q = url.parse(req.url, true);
    var qData = q.query;

    var gameID = qData.gameID;

    dbController.getGamePosition(dbConnection, gameID, function(data) {
        let response = new ResponseDict(400, data);
        res.json(response.dictionary);
    });

});

app.get('/attemptLogin', (req, res) => {
    var q = url.parse(req.url, true);
    var qData = q.query;

    dbController.checkLoginDetails(dbConnection, qData.username, qData.password, function(data) {

        if (data == "FAILURE") {
            let response = new ResponseDict(400, data);
            res.json(response.dictionary);
        } else {
            let claims = data;
            let token = jwt.create(claims, jwtKey);
            token.setExpiration(); // remove expiration
            let response = new ResponseDict(400, JSON.stringify({token: token.compact(), user_info: data}));
            res.json(response.dictionary);
        }
    });
});

app.get('/attemptSignUp', (req, res) => {
    var q = url.parse(req.url, true);
    var qData = q.query;

    dbController.checkUsernameExists(dbConnection, qData.username, function(data) {
        if (data) {
            let response = new ResponseDict(400, JSON.stringify({status: "FAILURE"}));
            res.json(response.dictionary);
        } else {

            dbController.createUser(dbConnection, qData.username, qData.password, function(insertID) {
                let claims = {userID: insertID, username: qData.username};
                let token = jwt.create(claims, jwtKey);
                token.setExpiration(); // remove expiration
                let response = new ResponseDict(400, JSON.stringify({status: "SUCCESS", token: token.compact(), user_info: claims}));
                res.json(response.dictionary);
            });
        }
    });
});

// check if a username is taken
app.get('/checkUsernameExists', (req, res) => {

    var q = url.parse(req.url, true);
    var qData = q.query;

    dbController.checkUsernameExists(dbConnection, qData.username, function(data) {
        let response = new ResponseDict(400, data);
        res.json(response.dictionary);
    });
});

// request via ../makeMove?move=[sq1][sq2]
app.get('/makeMove', (req, res) => {

    var q = url.parse(req.url, true);
    var qData = q.query;

    var gameID = qData.gameID;
    var moveFrom = qData.moveFrom;
    var moveTo = qData.moveTo;

    gameController.processMove(dbConnection, gameID, moveFrom, moveTo, function(msg) {

        let response = new ResponseDict(400, msg);
        res.json(response.dictionary);
    });

});

 app.listen(port, () => console.log(`Server listening on ${port} ...`));